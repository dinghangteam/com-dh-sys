package com.dh.sys.sysuser.dao.impl;

import java.util.List;
import com.dh.sys.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.sysuser.dao.SysUserDao;
import com.dh.sys.sysuser.model.SysUser;
import com.dh.sys.sysuser.model.SysUserCriteria;

@Repository("SysUserDao")
public class SysUserDaoImpl extends BaseDao implements SysUserDao {


        public SysUserDaoImpl() {
            super();
        }


        public int countByExample(SysUserCriteria example) {
            Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("sys_user.ibatorgenerated_countByExample", example);
            return count;
        }


        public int deleteByExample(SysUserCriteria example) {
            int rows = getSqlMapClientTemplate().delete("sys_user.ibatorgenerated_deleteByExample", example);
            return rows;
        }


        public int deleteByPrimaryKey(String id) {
        SysUser key = new SysUser();
            key.setId(id);
            int rows = getSqlMapClientTemplate().delete("sys_user.ibatorgenerated_deleteByPrimaryKey", key);
            return rows;
        }


        public void insert(SysUser record) {
            getSqlMapClientTemplate().insert("sys_user.ibatorgenerated_insert", record);
        }


        public void insertSelective(SysUser record) {
            getSqlMapClientTemplate().insert("sys_user.ibatorgenerated_insertSelective", record);
        }


        @SuppressWarnings("unchecked")
        public List<SysUser> selectByExample(SysUserCriteria example,Integer page, Integer rows) {
            List<SysUser> list = getSqlMapClientTemplate().queryForList("sys_user.ibatorgenerated_selectByExample", example,page, rows);
            return list;
        }

        @SuppressWarnings("unchecked")
        public List<SysUser> selectByExample(SysUserCriteria example) {
            List<SysUser> list = getSqlMapClientTemplate().queryForList("sys_user.ibatorgenerated_selectByExample", example);
            return list;
        }


        public SysUser selectByPrimaryKey(String id) {
            SysUser key = new SysUser();
            key.setId(id);
            SysUser record = (SysUser) getSqlMapClientTemplate().queryForObject("sys_user.ibatorgenerated_selectByPrimaryKey", key);
            return record;
        }


        public int updateByExampleSelective(SysUser record, SysUserCriteria example) {
            UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
            int rows = getSqlMapClientTemplate().update("sys_user.ibatorgenerated_updateByExampleSelective", parms);
            return rows;
        }


        public int updateByExample(SysUser record, SysUserCriteria example) {
            UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
            int rows = getSqlMapClientTemplate().update("sys_user.ibatorgenerated_updateByExample", parms);
            return rows;
        }


        public int updateByPrimaryKeySelective(SysUser record) {
            int rows = getSqlMapClientTemplate().update("sys_user.ibatorgenerated_updateByPrimaryKeySelective", record);
            return rows;
        }


        public int updateByPrimaryKey(SysUser record) {
            int rows = getSqlMapClientTemplate().update("sys_user.ibatorgenerated_updateByPrimaryKey", record);
            return rows;
        }


        private static class UpdateByExampleParms extends SysUserCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysUserCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
  }
}