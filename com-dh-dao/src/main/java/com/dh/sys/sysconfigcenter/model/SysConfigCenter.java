package com.dh.sys.sysconfigcenter.model;

import java.io.Serializable;

public class SysConfigCenter implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 主键
    */
    private String id;


    /**
    * 系统类型
    */
    private Integer sysType;


    /**
    * 描述
    */
    private String describeContent;


    /**
    * JSON串
    */
    private String jsonStr;


    /**
    * 创建时间
    */
    private String createDate;


    /**
    * 修改时间
    */
    private String updateDate;


    /**
    * 参数设置
    */
    private String paramSet;


    /**
    * 创建人
    */
    private String createUser;


    /**
    * 修改人
    */
    private String updateUser;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public Integer getSysType() {
        return sysType;
    }

    public void setSysType(Integer sysType) {
        this.sysType = sysType;
    }
    
    public String getDescribeContent() {
        return describeContent;
    }

    public void setDescribeContent(String describeContent) {
        this.describeContent = describeContent;
    }
    
    public String getJsonStr() {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }
    
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    
    public String getParamSet() {
        return paramSet;
    }

    public void setParamSet(String paramSet) {
        this.paramSet = paramSet;
    }
    
    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
    

}