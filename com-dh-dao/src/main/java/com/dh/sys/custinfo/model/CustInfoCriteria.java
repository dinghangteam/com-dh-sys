package com.dh.sys.custinfo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class CustInfoCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public CustInfoCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected CustInfoCriteria(CustInfoCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("id is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("id is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("id =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("id <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("id >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("id >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("id <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("id <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("id like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("id not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("id in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("id not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("id between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("id not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andUserCodeIsNull() {
        addCriterion("user_code is null");
        return this;
    }

    public Criteria andUserCodeIsNotNull() {
        addCriterion("user_code is not null");
        return this;
    }

    public Criteria andUserCodeEqualTo(String value) {
        addCriterion("user_code =", value, "userCode");
        return this;
    }

    public Criteria andUserCodeNotEqualTo(String value) {
        addCriterion("user_code <>", value, "userCode");
        return this;
    }

    public Criteria andUserCodeGreaterThan(String value) {
        addCriterion("user_code >", value, "userCode");
        return this;
    }

    public Criteria andUserCodeGreaterThanOrEqualTo(String value) {
        addCriterion("user_code >=", value, "userCode");
        return this;
    }

    public Criteria andUserCodeLessThan(String value) {
        addCriterion("user_code <", value, "userCode");
        return this;
    }

    public Criteria andUserCodeLessThanOrEqualTo(String value) {
        addCriterion("user_code <=", value, "userCode");
        return this;
    }

    public Criteria andUserCodeLike(String value) {
        addCriterion("user_code like", value, "userCode");
        return this;
    }

    public Criteria andUserCodeNotLike(String value) {
        addCriterion("user_code not like", value, "userCode");
        return this;
    }

    public Criteria andUserCodeIn(List<String> values) {
        addCriterion("user_code in", values, "userCode");
        return this;
    }

    public Criteria andUserCodeNotIn(List<String> values) {
        addCriterion("user_code not in", values, "userCode");
        return this;
    }

    public Criteria andUserCodeBetween(String value1, String value2) {
        addCriterion("user_code between", value1, value2, "userCode");
        return this;
    }

    public Criteria andUserCodeNotBetween(String value1, String value2) {
        addCriterion("user_code not between", value1, value2, "userCode");
        return this;
    }

    
    public Criteria andUserNameIsNull() {
        addCriterion("user_name is null");
        return this;
    }

    public Criteria andUserNameIsNotNull() {
        addCriterion("user_name is not null");
        return this;
    }

    public Criteria andUserNameEqualTo(String value) {
        addCriterion("user_name =", value, "userName");
        return this;
    }

    public Criteria andUserNameNotEqualTo(String value) {
        addCriterion("user_name <>", value, "userName");
        return this;
    }

    public Criteria andUserNameGreaterThan(String value) {
        addCriterion("user_name >", value, "userName");
        return this;
    }

    public Criteria andUserNameGreaterThanOrEqualTo(String value) {
        addCriterion("user_name >=", value, "userName");
        return this;
    }

    public Criteria andUserNameLessThan(String value) {
        addCriterion("user_name <", value, "userName");
        return this;
    }

    public Criteria andUserNameLessThanOrEqualTo(String value) {
        addCriterion("user_name <=", value, "userName");
        return this;
    }

    public Criteria andUserNameLike(String value) {
        addCriterion("user_name like", value, "userName");
        return this;
    }

    public Criteria andUserNameNotLike(String value) {
        addCriterion("user_name not like", value, "userName");
        return this;
    }

    public Criteria andUserNameIn(List<String> values) {
        addCriterion("user_name in", values, "userName");
        return this;
    }

    public Criteria andUserNameNotIn(List<String> values) {
        addCriterion("user_name not in", values, "userName");
        return this;
    }

    public Criteria andUserNameBetween(String value1, String value2) {
        addCriterion("user_name between", value1, value2, "userName");
        return this;
    }

    public Criteria andUserNameNotBetween(String value1, String value2) {
        addCriterion("user_name not between", value1, value2, "userName");
        return this;
    }

    
    public Criteria andCreateTimeIsNull() {
        addCriterion("create_time is null");
        return this;
    }

    public Criteria andCreateTimeIsNotNull() {
        addCriterion("create_time is not null");
        return this;
    }

    public Criteria andCreateTimeEqualTo(String value) {
        addCriterion("create_time =", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeNotEqualTo(String value) {
        addCriterion("create_time <>", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeGreaterThan(String value) {
        addCriterion("create_time >", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeGreaterThanOrEqualTo(String value) {
        addCriterion("create_time >=", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeLessThan(String value) {
        addCriterion("create_time <", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeLessThanOrEqualTo(String value) {
        addCriterion("create_time <=", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeLike(String value) {
        addCriterion("create_time like", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeNotLike(String value) {
        addCriterion("create_time not like", value, "createTime");
        return this;
    }

    public Criteria andCreateTimeIn(List<String> values) {
        addCriterion("create_time in", values, "createTime");
        return this;
    }

    public Criteria andCreateTimeNotIn(List<String> values) {
        addCriterion("create_time not in", values, "createTime");
        return this;
    }

    public Criteria andCreateTimeBetween(String value1, String value2) {
        addCriterion("create_time between", value1, value2, "createTime");
        return this;
    }

    public Criteria andCreateTimeNotBetween(String value1, String value2) {
        addCriterion("create_time not between", value1, value2, "createTime");
        return this;
    }

    
    public Criteria andPasswordIsNull() {
        addCriterion("password is null");
        return this;
    }

    public Criteria andPasswordIsNotNull() {
        addCriterion("password is not null");
        return this;
    }

    public Criteria andPasswordEqualTo(String value) {
        addCriterion("password =", value, "password");
        return this;
    }

    public Criteria andPasswordNotEqualTo(String value) {
        addCriterion("password <>", value, "password");
        return this;
    }

    public Criteria andPasswordGreaterThan(String value) {
        addCriterion("password >", value, "password");
        return this;
    }

    public Criteria andPasswordGreaterThanOrEqualTo(String value) {
        addCriterion("password >=", value, "password");
        return this;
    }

    public Criteria andPasswordLessThan(String value) {
        addCriterion("password <", value, "password");
        return this;
    }

    public Criteria andPasswordLessThanOrEqualTo(String value) {
        addCriterion("password <=", value, "password");
        return this;
    }

    public Criteria andPasswordLike(String value) {
        addCriterion("password like", value, "password");
        return this;
    }

    public Criteria andPasswordNotLike(String value) {
        addCriterion("password not like", value, "password");
        return this;
    }

    public Criteria andPasswordIn(List<String> values) {
        addCriterion("password in", values, "password");
        return this;
    }

    public Criteria andPasswordNotIn(List<String> values) {
        addCriterion("password not in", values, "password");
        return this;
    }

    public Criteria andPasswordBetween(String value1, String value2) {
        addCriterion("password between", value1, value2, "password");
        return this;
    }

    public Criteria andPasswordNotBetween(String value1, String value2) {
        addCriterion("password not between", value1, value2, "password");
        return this;
    }

        }
}